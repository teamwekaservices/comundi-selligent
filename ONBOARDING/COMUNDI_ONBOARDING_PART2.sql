MERGE ACTION_COMUNDI_ONBOARDING AS T
USING (
	SELECT 
		T1.ID_LAM AS ID_PP_LAM,
		T1.ID_SESSION AS ID_SESSION_LAM, 
		T1.ID_RETOUR_LAM AS ID_RETOUR_LAM,
		T1.DATE_DEBUT AS DATE_DEBUT,
		T1.DATE_FIN AS DATE_FIN,
		T1.OPA_ACTIVITE,
		T2.ID AS ID_ACTION
	FROM Tmp_COMUNDI_Onboarding_part2 T1
	LEFT JOIN ACTION_COMUNDI_ONBOARDING T2 ON T1.ID_RETOUR_LAM = T2.ID_RETOUR_LAM
	WHERE SIM_PARSELINE_RESULT = 1 
) AS S
ON (T.ID = S.ID_ACTION ) 
WHEN MATCHED
	THEN 
		UPDATE SET 
		T.ID_SESSION_LAM=S.ID_SESSION_LAM,
		T.DATE_DEBUT=S.DATE_DEBUT,
		T.DATE_FIN=S.DATE_FIN,
		T.OPA_ACTIVITE =S.OPA_ACTIVITE,
		T.MODIFIED_DT=GETDATE();

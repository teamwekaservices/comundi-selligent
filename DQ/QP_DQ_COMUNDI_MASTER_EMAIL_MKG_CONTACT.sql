-- calcul du master email mkg pour  COMUNDI_EMAILS et qui se réplique sur COMUNDI_CONTACT


AS
BEGIN
SET NOCOUNT ON;

DROP INDEX IF EXISTS IDX_MASTER_EMAIL_MKG ON USERS_COMUNDI_EMAILS;

UPDATE USERS_COMUNDI_EMAILS  
SET MASTER_EMAIL_MKG = 0
WHERE MASTER_EMAIL_MKG = 1 OR MASTER_EMAIL_MKG IS NULL;

UPDATE USERS_COMUNDI_EMAILS  
SET MAIL_CODE = ID
WHERE MAIL_CODE IS NULL;

-- calcul du master 
UPDATE T1  
SET MASTER_EMAIL_MKG = 1  
FROM USERS_COMUNDI_EMAILS T1
WHERE 
T1.OPTOUT IS NULL 
AND (T1.BLACKLIST_NDD =0 OR T1.BLACKLIST_NDD IS NULL)
AND (T1.BOUNCEDT <= GETDATE()-15 OR T1.BOUNCEDT IS NULL) 
AND NOT EXISTS (SELECT 1 FROM DATA_COMUNDI_CONSENTEMENTS T3 WHERE T1.MAIL_CODE=T3.MAIL_CODE AND T3.EMAIL=0)
and created_dt < getdate()-30
and (dormeur_permanent is null or dormeur_permanent = 0)
and (DATE_FIN_ISOLEMENT is null or DATE_FIN_ISOLEMENT < getdate()-41 );


-- calcul de nouveaux dormeurs potentiels

-- calcul de nouveaux dormeurs potentiels
UPDATE T1 
SET MASTER_EMAIL_MKG = 0, DORMEUR_POTENTIEL = 1 
from USERS_COMUNDI_EMAILS T1 with (nolock)
where 
	T1.optout is null and 
	T1.DORMEUR_POTENTIEL is null
	and T1.derniere_activite_dt > DATEADD(yy,-1,getdate())
	and exists (
		select 1 from TAXONOMY_L170_T5 T2 with (nolock)
		WHERE 
			T1.ID = T2.USERID
			and (
				T2.CLICK_LASTDT < DATEADD(mm,-6,getdate())
				or 
				(CLICK_LASTDT IS NULL AND MAILS_SENT > 12)

--				(T2.CLICK_LASTDT < DATEADD(mm,-6,getdate()) or T2.CLICK_LASTDT is null) 
--				and 
--				(CLICK_LASTDT IS NULL AND MAILS_SENT > 12)
			)
			
	)
	and not exists (select 1 from DATA_COMUNDI_CONSENTEMENTS T3 with (nolock) 
			WHERE T1.MAIL_CODE = T3.MAIL_CODE AND T3.EMAIL=0)
	and not exists (select 1 from TAXONOMY_L170_T5 TE with (nolock) where TE.USERID=T1.ID AND CLICK_LASTDT > dateadd(mm,-6,getdate()) ) 
	and not exists (select 1 from DATA_COMUNDI_LEADS T2 with (nolock) where T1.ID=T2.MAIL_CODE and created_dt > dateadd(mm,-6,getdate()) )
	and not exists (select 1 from DATA_RETOUR_SESSION T3 with (nolock) where T1.ID=T3.MAIL_CODE_PARTICIPANT and DATE_SAISIE_RETOUR > dateadd(mm,-6,getdate())
	and not exists (select 1 from USERS_COMUNDI_PARTICIPANTS T4 with (nolock) 
			WHERE T1.MAIL = T4.MAIL and exists (select 1 from ARTICLES_COMUNDI_PARTICIPATIONS T5 with (nolock) where 
								T5.IDPARTICIPANTDENDREO=T4.IDPARTICIPANTDENDREO AND DATE_SAISIE > dateadd(mm,-6,getdate()))
	)
 )
;

 
-- reactivation des cliqueurs :
-- 24/08/2022 : #9534 : rajout de la date de réactivation 
update T1  
set dormeur_potentiel=null,dormeur_permanent=null,REACTIVATION_DT=getdate()
from USERS_COMUNDI_EMAILS T1 
where 
(dormeur_potentiel = 1 or dormeur_permanent = 1) 
and 
(
exists (select 1 from TAXONOMY_L170_T5 TE with (nolock) where TE.USERID=T1.ID AND CLICK_LASTDT > dateadd(mm,-6,getdate()) ) 
or 
exists (select 1 from DATA_COMUNDI_LEADS T2 with (nolock) where T1.ID=T2.MAIL_CODE and created_dt > dateadd(mm,-6,getdate()) )
or 
exists (select 1 from DATA_RETOUR_SESSION T3 with (nolock) where T1.ID=T3.MAIL_CODE_PARTICIPANT and DATE_SAISIE_RETOUR > dateadd(mm,-6,getdate()) )
or 
 exists (select 1 from USERS_COMUNDI_PARTICIPANTS T4 with (nolock) 
			WHERE T1.MAIL = T4.MAIL and exists (select 1 from ARTICLES_COMUNDI_PARTICIPATIONS T5 with (nolock) where 
								T5.IDPARTICIPANTDENDREO=T4.IDPARTICIPANTDENDREO AND DATE_SAISIE > dateadd(mm,-6,getdate()))
	)
)


 
-- calcul du master 
UPDATE T1  
SET MASTER_EMAIL_MKG = 1  
FROM USERS_COMUNDI_EMAILS T1
WHERE 
T1.OPTOUT IS NULL 
AND (T1.BLACKLIST_NDD =0 OR T1.BLACKLIST_NDD IS NULL)
AND (T1.BOUNCEDT <= GETDATE()-15 OR T1.BOUNCEDT IS NULL) 
AND NOT EXISTS (SELECT 1 FROM DATA_COMUNDI_CONSENTEMENTS T3 WHERE T1.MAIL_CODE=T3.MAIL_CODE AND T3.EMAIL=0)
and created_dt < getdate()-30
and (dormeur_permanent is null or dormeur_permanent = 0)
and (DATE_FIN_ISOLEMENT is null or DATE_FIN_ISOLEMENT < getdate()-41 );


		
CREATE INDEX IDX_MASTER_EMAIL_MKG ON USERS_COMUNDI_EMAILS (MASTER_EMAIL_MKG);



---
--- POUR LES CONTACTS


DROP INDEX IF EXISTS IDX_MASTER_EMAIL_MKG ON USERS_COMUNDI_CONTACT;

UPDATE USERS_COMUNDI_CONTACT  
SET MASTER_EMAIL_MKG = 0
WHERE MASTER_EMAIL_MKG = 1 OR MASTER_EMAIL_MKG IS NULL;


UPDATE T1  
SET MASTER_EMAIL_MKG = 1  
FROM USERS_COMUNDI_CONTACT T1
WHERE 
EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T3 WHERE T1.MAIL_CODE=T3.MAIL_CODE AND T3.MASTER_EMAIL_MKG=1); 		
 		 		
CREATE INDEX IDX_MASTER_EMAIL_MKG ON USERS_COMUNDI_CONTACT (MASTER_EMAIL_MKG);


END
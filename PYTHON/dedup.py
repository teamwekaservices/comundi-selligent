# Filter Cols
from ctypes import cast
import pandas as pd
import csv


def check_string_to_float(s):
    try:
        float(s)
        return True
    except:
        return False
        
        
def get_cat_from_sscat(s):
    try:
      categorie_index=s.rsplit("_",1)
      sscat=categorie_index[0]
      return sscat
    except:
        return ''


f=pd.read_csv("export_produits_comundi.csv", sep=";")

# On ne garde que les modules
f = f[f["family"] == 'module']

# on filtre les colonnes
keep_col = ['sku','categories', 'souscategorie_principale', 'tarif_ht', 'titre-web','postit','description_courte','sous_titre-web','url_finale','eligible_cpf','statut_workflow']

new_f = f[keep_col]

#On convertie la colonne eligible_cpf en bool
new_f['eligible_cpf'] = new_f['eligible_cpf'].apply(bool)

new_f.to_csv("dedupcsv.csv", sep=';', index=False)

new_f = new_f.copy()

# Filtre une categorie
new_f['categories'] = new_f['souscategorie_principale'].apply(lambda x: get_cat_from_sscat(x) )

# Filtre un prix ex : PF91
new_f['tarif_ht'] = new_f['tarif_ht'].apply(lambda x: x.split(';')[0] if ( not isinstance(x, float)  and len(x.split(';'))>1) else x )

# Remove non float values in field
new_f['tarif_ht'] = new_f['tarif_ht'].apply(lambda x: x if check_string_to_float(x) else '' )

#remove retour chariots
new_f['description_courte']=new_f['description_courte'].replace(to_replace='\n', value=' ', regex=True)
new_f['sous_titre-web']=new_f['sous_titre-web'].replace(to_replace='\n', value=' ', regex=True)

#renommage colonne titre-web en titre_web
new_f.columns = ['sku','categories', 'souscategorie_principale', 'tarif_ht', 'titre_web','postit','description_courte','sous_titre_web','url_finale','eligible_cpf','statut_workflow' ]

new_f.to_csv("dedupcsv.csv", sep=';', index=False, quoting=csv.QUOTE_ALL)

new_f = new_f.drop_duplicates(subset=['sku'], keep='first')

new_f.to_csv("athena_dedup.csv", sep=';', index=False, quoting=csv.QUOTE_ALL)

# Deduplication
#with open('dedupcsv.csv','r') as in_file, open('athena_dedup.csv','w') as out_file:
#    seen = set()
#    for line in in_file:
#        if line in seen: continue
#
#       seen.add(line)
#        out_file.write(line)

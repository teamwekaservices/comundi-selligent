AS
BEGIN
 /* ******************* Documentation Template (max 200 chars per line) ******************** 
  -- DID - Author: Author 
  -- DID - CreationDate: Creation Date 
  -- DID - Version: 0.1.0 
  -- DID - Description: Description 
  -- DID - Exceptions: Exceptions 
  -- DID - BusinessRules: Rule 
  -- DID - LastModifiedBy: LastModifiedBy 
 ******************* Documentation Template (max 200 chars per line) ********************  */ 
SET NOCOUNT ON;

MERGE dbo.ARTICLES_COMUNDI_ADF AS T
	USING (
		SELECT 	ID,
				NUMERO_COMPLET_MODULE,
				NUMERO_COMPLET_ADF,
				ID_ETAPE_PROCESS,
				C_TYPE_ADF_SESSION,
				INTITULE_ADF,
				INTITULE_MODULE,
				C_PROFIT_CENTER,
				DATE_DEBUT,
				DATE_FIN,
				ID_COMMERCIAL,
				C_COMMERCIAL_2_ID,
				C_SUPERVISE_PAR_ID,
				ID_ADMINISTRATEUR,
				C_TYPE_PRODUIT,
				C_ZONE_GEO_ADF,
				CO_MODAL,
				C_ADF_MULTI_MODULES_CARREFOUR_ETC,
				C_SESSION_GARANTIE,
				PETITE_SESSION,
				LIEU_RS,
				LIEU_ADRESSE,
				LIEU_CP,
				LIEU_VILLE,
				NBRE_IC,
				NBRE_PLACE_RESTANTE
			FROM DBO.TMP_COMUNDI_ADF
		) AS S
	ON (T.CODE_ADF = S.NUMERO_COMPLET_ADF AND NULLIF(S.ID,'') IS NOT NULL) 
	WHEN NOT MATCHED BY TARGET
		THEN 
			INSERT(
				CREATED_DT,
				MODIFIED_DT,
				LANGUAGE,
				CODE_MODULE,
				CODE_ADF,
				STATUT_SESSION,
				TYPE_OPERATION,
				NOM_FORMATION,
				NOM_CESSION,
				CENTRE_PROFIT,
				DATE_DEBUT,
				DATE_FIN,
				ID_COMMERCIAL,
				ID_COMMERCIAL_2,
				ID_SUPERVISEUR,
				ID_ADMINISTRATEUR,
				TYPE_PRODUIT,
				ZONE_GEOGRAPHIQUE,
				SESSION_BLENDED,
				IS_PRODUIT_MULTI_MODULES,
				SESSION_GARANTIE,
				PETITE_SESSION,
				LIEU_SESSION_RS1,
				LIEU_SESSION_ADRESSE1,
				LIEU_SESSION_CP,
				LIEU_SESSION_VILLE,
				STATUT_INSCRIPTION,
				NB_PLACE_RESTANTES
			)
			VALUES(
				GETDATE(),
				GETDATE(),
				'FR',
				S.NUMERO_COMPLET_MODULE,
				S.NUMERO_COMPLET_ADF,
				S.ID_ETAPE_PROCESS,
				S.C_TYPE_ADF_SESSION,
				S.INTITULE_ADF,
				S.INTITULE_MODULE,
				S.C_PROFIT_CENTER,
				S.DATE_DEBUT,
				S.DATE_FIN,
				S.ID_COMMERCIAL,
				S.C_COMMERCIAL_2_ID,
				S.C_SUPERVISE_PAR_ID,
				S.ID_ADMINISTRATEUR,
				S.C_TYPE_PRODUIT,
				S.C_ZONE_GEO_ADF,
				S.CO_MODAL,
				S.C_ADF_MULTI_MODULES_CARREFOUR_ETC,
				S.C_SESSION_GARANTIE,
				S.PETITE_SESSION,
				S.LIEU_RS,
				S.LIEU_ADRESSE,
				S.LIEU_CP,
				S.LIEU_VILLE,
				S.NBRE_IC,
				S.NBRE_PLACE_RESTANTE
			) 
	WHEN MATCHED 
		THEN 
			UPDATE SET T.MODIFIED_DT = GETDATE(),
				T.CODE_MODULE = S.NUMERO_COMPLET_MODULE,
				T.CODE_ADF = S.NUMERO_COMPLET_ADF,
				T.STATUT_SESSION = S.ID_ETAPE_PROCESS,
				T.TYPE_OPERATION = S.C_TYPE_ADF_SESSION,
				T.NOM_FORMATION = S.INTITULE_ADF,
				T.NOM_CESSION = S.INTITULE_MODULE,
				T.CENTRE_PROFIT = S.C_PROFIT_CENTER,
				T.DATE_DEBUT = S.DATE_DEBUT,
				T.DATE_FIN = S.DATE_FIN,
				T.ID_COMMERCIAL = S.ID_COMMERCIAL,
				T.ID_COMMERCIAL_2 = S.C_COMMERCIAL_2_ID,
				T.ID_SUPERVISEUR = S.C_SUPERVISE_PAR_ID,
				T.ID_ADMINISTRATEUR = S.ID_ADMINISTRATEUR,
				T.TYPE_PRODUIT = S.C_TYPE_PRODUIT,
				T.ZONE_GEOGRAPHIQUE = S.C_ZONE_GEO_ADF,
				T.SESSION_BLENDED = S.CO_MODAL,
				T.IS_PRODUIT_MULTI_MODULES = S.C_ADF_MULTI_MODULES_CARREFOUR_ETC,
				T.SESSION_GARANTIE = S.C_SESSION_GARANTIE,
				T.PETITE_SESSION = S.PETITE_SESSION,
				T.LIEU_SESSION_RS1 = S.LIEU_RS,
				T.LIEU_SESSION_ADRESSE1 = S.LIEU_ADRESSE,
				T.LIEU_SESSION_CP = S.LIEU_CP,
				T.LIEU_SESSION_VILLE = S.LIEU_VILLE,
				T.STATUT_INSCRIPTION = S.NBRE_IC,
				T.NB_PLACE_RESTANTES = S.NBRE_PLACE_RESTANTE;
				
END
AS
BEGIN
 /* ******************* Documentation Template (max 200 chars per line) ******************** 
  -- DID - Author: Author 
  -- DID - CreationDate: Creation Date 
  -- DID - Version: 0.1.0 
  -- DID - Description: Description 
  -- DID - Exceptions: Exceptions 
  -- DID - BusinessRules: Rule 
  -- DID - LastModifiedBy: LastModifiedBy 
 ******************* Documentation Template (max 200 chars per line) ********************  */ 
SET NOCOUNT ON;

	MERGE dbo.ARTICLES_COMUNDI_MODULE AS T
	USING (
		SELECT 	ID,
				NUMERO_COMPLET,
				C_SKU_COMUNDI,
				C_MARQUE,
				C_TYPE_PRODUIT,
				C_PRODUIT_MULTI_MODULES,
				C_CENTRE_DE_PROFIT,
				C_CREE_PAR_ID,
				PRIX,
				NOUVEAUTE
			FROM DBO.TMP_COMUNDI_MODULE
		) AS S
	ON (T.CODE_MODULE = S.NUMERO_COMPLET AND NULLIF(S.ID,'') IS NOT NULL) 
	WHEN NOT MATCHED BY TARGET
		THEN 
			INSERT(
				CREATED_DT,
				MODIFIED_DT,
				LANGUAGE,
				CODE_MODULE,
				CODE_PRODUIT_LAM,
				MARQUE,
				TYPE_PRODUIT,
				IS_PRODUIT_MULTI_MODULES,
				CENTRE_PROFIT,
				CREE_PAR,
				PRIX_PRODUIT,
				IS_NOUVEAUTE
			)
			VALUES(
				GETDATE(),
				GETDATE(),
				'FR',
				S.NUMERO_COMPLET,
				S.C_SKU_COMUNDI,
				S.C_MARQUE,
				S.C_TYPE_PRODUIT,
				S.C_PRODUIT_MULTI_MODULES,
				S.C_CENTRE_DE_PROFIT,
				S.C_CREE_PAR_ID,
				S.PRIX,
				S.NOUVEAUTE
			) 
	WHEN MATCHED 
		THEN 
			UPDATE SET T.MODIFIED_DT = GETDATE(),
				T.CODE_MODULE = S.NUMERO_COMPLET,
				T.CODE_PRODUIT_LAM = S.C_SKU_COMUNDI,
				T.MARQUE = S.C_MARQUE,
				T.TYPE_PRODUIT = S.C_TYPE_PRODUIT,
				T.IS_PRODUIT_MULTI_MODULES = S.C_PRODUIT_MULTI_MODULES,
				T.CENTRE_PROFIT = S.C_CENTRE_DE_PROFIT,
				T.CREE_PAR = S.C_CREE_PAR_ID,
				T.PRIX_PRODUIT = S.PRIX,
				T.IS_NOUVEAUTE = S.NOUVEAUTE;

END
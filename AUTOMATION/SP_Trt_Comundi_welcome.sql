-- ticket https://redmine.weka-ssc.fr/issues/7649
-- specs : https://docs.google.com/document/d/1Ku8aZod28j_WEQqmgei0s9EoKeNfrIlcabCl43Q8jtk/edit#



--	SELECT DISTINCT(EMAIL),getdate(),4,SUBSTRING(EMAIL,CHARINDEX ('@',EMAIL)+1,1000) --	FROM TMP_COMUNDI_LEADS 


-- Condition 1 : 
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT,
	MAX(
	CASE 
		WHEN TA.NOM_FORMATION = '' THEN TA.NOM_FORMATION
		ELSE T1.CONTENU 
	END),
	'WELCOME_1',MAX(CIVILITE),MAX(NOM),MAX(PRENOM)  
FROM DATA_COMUNDI_LEADS T1
INNER JOIN ARTICLES_SESSION_FORMATION TA ON T1.CODE_PRODUIT = TA.ID_SESSION
WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-30)
AND FORMULAIRE = 72
AND CODE_PRODUIT IS NOT NULL
AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-30
GROUP BY T1.MAIL_CODE, T1.CREATED_DT


-- Condition 1 bis : 
-- rajout point du 21/10/21
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT,MAX(TA.NOM_FORMATION),'WELCOME_10',MAX(CIVILITE),MAX(NOM),MAX(PRENOM)   FROM DATA_COMUNDI_LEADS T1INNER JOIN ARTICLES_SESSION_FORMATION TA ON T1.CODE_PRODUIT = TA.ID_FORMATIONWHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND exists (SELECT 1 FROM DATA_COMUNDI_LEADS_FORM T5 WHERE T1.FORMULAIRE = T5.ID AND CODE LIKE '%devis%')AND CODE_PRODUIT IS NOT NULLAND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT

-- Condition 1 ter : 
-- rajout point du 21/10/21
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT, MAX(T1.CONTENU),'WELCOME_11',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND exists (SELECT 1 FROM DATA_COMUNDI_LEADS_FORM T5 WHERE T1.FORMULAIRE = T5.ID AND CODE LIKE '%capsule%')AND CONTENU IS NOT NULL 
AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT


-- Condition 2 : 
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT ,'WELCOME_2',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND FORMULAIRE = 1AND CONTENU IN ('Poser une question sur une formation','Obtenir des renseignements administratifs sur Comundi','Demander une formation sur mesure dans vos locaux','Demande d''information ou devis pour un MOOC','Demande d''information sur l''actualité','Demande d''information sur le Digital learning')AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT;

-- Condition 3 : 
-- modification point du 21/10/21, on prend tout ce qui contient magazine
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT,'Magazine des compétences','WELCOME_3',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND exists (SELECT 1 FROM DATA_COMUNDI_LEADS_FORM T5 WHERE T1.FORMULAIRE = T5.ID AND CODE LIKE '%Magazine%')AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-2
GROUP BY T1.MAIL_CODE, T1.CREATED_DT

-- Condition 6 : 
-- modification point du 21/10/21, on prend tout ce qui contient webinar ou webinaire
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT, MAX(T1.CONTENU),'WELCOME_6',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND exists (SELECT 1 FROM DATA_COMUNDI_LEADS_FORM T5 WHERE T1.FORMULAIRE = T5.ID AND (CODE LIKE '%webinar%' OR CODE LIKE '%webinaire%'))AND CONTENU IS NOT NULL 
AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT



-- Condition 7 : 
-- le 21/10/2021 on a désactivé cette condition pour tout mettre dans le WELCOME_6 avec le %webinar%
--INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE)--SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT,MAX(TA.NOM_FORMATION),'WELCOME_7'  --FROM DATA_COMUNDI_LEADS T1--INNER JOIN ARTICLES_SESSION_FORMATION TA ON T1.CODE_PRODUIT = TA.ID_SESSION--WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)--AND FORMULAIRE = 76--AND CODE_PRODUIT IS NOT NULL--AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
--AND T1.CREATED_DT > getdate()-2--GROUP BY T1.MAIL_CODE, T1.CREATED_DT


-- Condition 4 : 
-- modification point du 21/10/21, on prend tout ce qui contient livre et blanc
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT, MAX(T1.CONTENU),'WELCOME_4',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND exists (SELECT 1 FROM DATA_COMUNDI_LEADS_FORM T5 WHERE T1.FORMULAIRE = T5.ID AND CODE LIKE '%livre%blanc%')AND CONTENU IS NOT NULL 
AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT


-- Condition Location de fichier 
-- ajout du 04/11/2021 
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT, MAX(T1.CONTENU),'WELCOME_12',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND exists (SELECT 1 FROM DATA_COMUNDI_LEADS_FORM T5 WHERE T1.FORMULAIRE = T5.ID AND CODE LIKE '%location%fichier%')AND CONTENU IS NOT NULL 
AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT;



-- Condition 8 : 
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT, 'Guide du CPF','WELCOME_8',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND FORMULAIRE = 37AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT;

-- Condition 9 : 
-- le 21/10/2021 on a désactivé cette condition pour tout mettre dans le WELCOME_6 avec le %webinar%
--INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE)--SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT, MAX(T1.CONTENU),'WELCOME_9'--FROM DATA_COMUNDI_LEADS T1--WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)--AND FORMULAIRE = 62--AND CONTENU IS NOT NULL 
--AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
--AND T1.CREATED_DT > getdate()-2--GROUP BY T1.MAIL_CODE, T1.CREATED_DT


-- Condition 5 : 
-- modification point du 21/10/21, on prend tout ce qui contient podcast
INSERT INTO ACTION_COMUNDI_WELCOME (LISTID,CREATED_DT,USERID,LEAD_DT,TXT_PERSO,ACTIONCODE,CIVILITE,NOM,PRENOM)SELECT DISTINCT 170,getdate(),T1.MAIL_CODE, T1.CREATED_DT, MAX(T1.CONTENU),'WELCOME_5',MAX(CIVILITE),MAX(NOM),MAX(PRENOM) FROM DATA_COMUNDI_LEADS T1WHERE EXISTS (SELECT 1 FROM USERS_COMUNDI_EMAILS T2 with (nolock) WHERE T1.MAIL_CODE = T2.ID AND SOURCE = 4 AND CREATED_DT> getdate()-1)AND exists (SELECT 1 FROM DATA_COMUNDI_LEADS_FORM T5 WHERE T1.FORMULAIRE = T5.ID AND CODE LIKE '%podcast%')AND CONTENU IS NOT NULL 
AND NOT EXISTS (select 1 FROM ACTION_COMUNDI_WELCOME T3 WHERE T1.MAIL_CODE = T3.USERID)
AND T1.CREATED_DT > getdate()-2GROUP BY T1.MAIL_CODE, T1.CREATED_DT;


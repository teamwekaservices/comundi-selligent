
-- sélection, concaténation et insertion des sessions par formateur dans la table d'actions
INSERT INTO ACTION_COMUNDI_FORMATEUR (CREATED_DT, ACTIONCODE, USERID, ID_SESSION,NOM_FORMATION)
SELECT GETDATE(),'COMUNDI_FORMATEUR',T1.ID, T3.ID,T3.NOM_FORMATION
FROM USERS_COMUNDI_LAM T1 with (nolock)
INNER JOIN DATA_RETOUR_SESSION T2 with (nolock) ON T1.NUM_CLIENT_CONTACT = T2.ID_LAM AND T2.STATUT_RETOUR=10
INNER JOIN ARTICLES_SESSION_FORMATION T3 with (nolock) ON T2.FORMATION_SESSION_ID = T3.ID AND T3.TYPE_SESSION IN (1,5,8) AND CAST(T3.DATE_DEBUT as DATE) = cast(getdate()+2 as date) AND T3.STATUT_SESSION=0
WHERE 
T1.MAIL <> 'NR'
AND T1.OPTOUT IS NULL ; 


------------------------------------------------------------------------------------------------------------------------
--Ajout du 23/09/22 - ticket #9693 
------------------------------------------------------------------------------------------------------------------------
--suppression de la table temporaire si elle existe 
-- sélection, concaténation et insertion des sessions par formateur dans la table d'actions
INSERT INTO ACTION_COMUNDI_FORMATEUR (CREATED_DT, ACTIONCODE, USERID, ID_SESSION,NOM_FORMATION)
SELECT GETDATE(),'COMUNDI_FORMATEUR_J-28',T1.ID, T3.ID,T3.NOM_FORMATION
FROM USERS_COMUNDI_LAM T1 with (nolock)
INNER JOIN DATA_RETOUR_SESSION T2 with (nolock) ON T1.NUM_CLIENT_CONTACT = T2.ID_LAM AND T2.STATUT_RETOUR=10
INNER JOIN ARTICLES_SESSION_FORMATION T3 with (nolock) ON T2.FORMATION_SESSION_ID = T3.ID AND T3.TYPE_SESSION IN (1,5,8) AND CAST(T3.DATE_DEBUT as DATE) = cast(getdate()+28 as date) AND T3.STATUT_SESSION=0
WHERE 
T1.MAIL <> 'NR'
AND T1.OPTOUT IS NULL ; 
------------------------------------------------------------------------------------------------------------------------
-- fin ajout du 23/09/22 
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--Ajout du 10/10/22 - ticket #9730 
------------------------------------------------------------------------------------------------------------------------
--suppression de la table temporaire si elle existe 
-- sélection, concaténation et insertion des sessions par formateur dans la table d'actions
INSERT INTO ACTION_COMUNDI_FORMATEUR (CREATED_DT, ACTIONCODE, USERID, ID_SESSION,NOM_FORMATION)
SELECT GETDATE(),'COMUNDI_FORMATEUR_J1',T1.ID, T3.ID,T3.NOM_FORMATION
FROM USERS_COMUNDI_LAM T1 with (nolock)
INNER JOIN DATA_RETOUR_SESSION T2 with (nolock) ON T1.NUM_CLIENT_CONTACT = T2.ID_LAM AND T2.STATUT_RETOUR=10
INNER JOIN ARTICLES_SESSION_FORMATION T3 with (nolock) ON T2.FORMATION_SESSION_ID = T3.ID AND T3.TYPE_SESSION IN (1,5,8) AND CAST(T3.DATE_FIN as DATE) = cast(getdate()-1 as date) AND T3.STATUT_SESSION in (0,10)
WHERE 
T1.MAIL <> 'NR'
AND T1.OPTOUT IS NULL ; 
------------------------------------------------------------------------------------------------------------------------
-- fin ajout du 10/10/22 
------------------------------------------------------------------------------------------------------------------------


-- suppression des données qui ont été envoyées il y a plus 60 jours DELETE FROM ACTION_COMUNDI_FORMATEUR WHERE CREATED_DT < GETDATE()-60;

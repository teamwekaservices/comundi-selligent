-- demande initiale : https://redmine.weka-ssc.fr/issues/9321?issue_count=11&issue_position=2&next_issue_id=9133&prev_issue_id=9430
-- PARTIE 2 sur les achats de fichiers pour suivre le taux de rétention des contacts
-- export vers le FTP/S3 : FLUX_152_COMUNDI_KPI_ACHATS_FICHIERS.csv dans /home/athenaftqj-SELLPRO/WEBCOMUNDI/TODO/


-- on filtre sur les emails qui ont un ACHAT_SOURCE renseigné et une date de création dans un intervalle de 24 mois 

--- taux de rétention 
SELECT ACHAT_SOURCE,FORMAT(max(cast(CREATED_DT as date)),'yyyy-MM-dd') as CREATED_DT,  count(*) as QT_ACHETE, count(T3.ID) AS NB_INSCRIPTIONS, REPLACE(sum (PRIX_FORMATION),',','.' )AS CA_HT_BRUT
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > T1.CREATED_DT) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,1,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M1'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,2,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M2'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,3,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M3'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,4,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M4'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,5,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M5'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,6,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M6'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,7,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M7'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,8,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M8'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,9,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M9'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,10,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M10'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,11,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M11'
,FORMAT(cast(sum(
	case when (T2.CLICK_LASTDT > dateadd(mm,12,T1.CREATED_DT)) then 1 else 0 end
) as float)/count(*),'0.0000') AS 'M12'
from USERS_COMUNDI_EMAILS T1
INNER JOIN TAXONOMY_L170_T5 T2 on T1.ID = T2.USERID
INNER JOIN DATA_RETOUR_SESSION T3 on T1.ID = T3.MAIL_CODE_PARTICIPANT
WHERE ACHAT_SOURCE is not null
AND T1.SUBSCRIBE_SOURCE in ('ACHATS MKG', 'RH FORMATION')
AND T3.STATUT_RETOUR not in (4,5,8)
AND T1.CREATED_DT> '2022-06-08'
AND T3.DATE_SAISIE_RETOUR > T1.CREATED_DT
AND T3.PRIX_FORMATION>0
group by ACHAT_SOURCE, SUBSCRIBE_SOURCE;

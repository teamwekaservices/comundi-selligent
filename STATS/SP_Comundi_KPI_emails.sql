-- demande initiale : https://redmine.weka-ssc.fr/issues/9321?issue_count=11&issue_position=2&next_issue_id=9133&prev_issue_id=9430
-- PARTIE 1 : KPI sur les volumétries d'emails
-- export vers le FTP/S3 : FLUX_152_COMUNDI_KPI_EMAILS.csv dans /home/athenaftqj-SELLPRO/WEBCOMUNDI/TODO/

-- création de la date dans les stats : 
if (select 1 from DATA_COMUNDI_KPI_DATASTUDIO where DT=cast(GETDATE() as date)) is null
INSERT INTO DATA_COMUNDI_KPI_DATASTUDIO (DT) VALUES (getdate())


--Le volume total d’adresses email en BDD.
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_TOTAL= (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock))
WHERE DT = cast(getdate() as date)

--Le volume total de contacts optin 
--=> tous les optins : qui ne sont pas opt out consentement EMAIL (175259)
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_OPTIN =(select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
where not exists (
	select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) 
	where T1.MAIL = T2.MAIL AND EMAIL = 0
)  and optout is null )
WHERE DT = cast(getdate() as date)

--Le volume total de contacts optout 
--=> tous les consentements négatifs au canal EMAIL (devrait être la soustraction entre total de la BDD et les optins)(60807)
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_OPTOUT =(select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
where exists (
	select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) 
	where T1.MAIL = T2.MAIL AND EMAIL = 0
) OR  optout is not null)
WHERE DT = cast(getdate() as date)

--Le volume total de contacts optin actifs - de 4 mois 
--=> qui a cliqué dans un email dans les 4 derniers mois (LAST_CLICK_DT) (8437)
--> 22/08/22 : rajout notion de pas optout marketing 
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_ACTIFS_4M = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
	where exists (
		select 1 from TAXONOMY_L170_T5 T2 with (nolock) 
		where T1.ID = T2.USERID AND CLICK_LASTDT>=DATEADD(month,-4,GETDATE()) 
	)  and optout is null
	and not exists (select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) where T1.MAIL = T2.MAIL AND EMAIL = 0) )
WHERE DT = cast(getdate() as date)

--Le volume total de contacts optin actifs  de 4 mois et moins d'un an 
--=> qui a cliqué dans un email entre les 4 et 12 derniers mois (LAST_CLICK_DT) (9060)
--> 22/08/22 : rajout notion de pas optout marketing 
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_ACTIFS_4M_12M = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
	where exists (
		select 1 from TAXONOMY_L170_T5 T2 with (nolock) 
		where T1.ID = T2.USERID AND CLICK_LASTDT<DATEADD(month,-4,GETDATE()) AND CLICK_LASTDT>=DATEADD(month,-12,GETDATE()) 
	)  and optout is null
	and not exists (select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) where T1.MAIL = T2.MAIL AND EMAIL = 0))
WHERE DT = cast(getdate() as date)

-- AJOUT le 22/08/22 Le volume total de contacts optin actifs  il y a plus de 12 mois  
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_ACTIFS_12M_PLUS = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
	where exists (
		select 1 from TAXONOMY_L170_T5 T2 with (nolock) 
		where T1.ID = T2.USERID AND CLICK_LASTDT<DATEADD(month,-12,GETDATE()) 
	)  and optout is null
	and not exists (select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) where T1.MAIL = T2.MAIL AND EMAIL = 0))
WHERE DT = cast(getdate() as date)


--Le volume total de contacts dormeurs potentiels optin 
--=> DORMEUR_POTENTIEL = 1 ET DORMEUR_PERMANENT = 0
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_OPTIN_DORMEUR_POTENTIEL = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
	where DORMEUR_POTENTIEL=1 and (DORMEUR_PERMANENT = 0 or DORMEUR_PERMANENT is null)
	and not exists (
		select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) 
		where T1.MAIL = T2.MAIL AND EMAIL = 0
	)  and optout is null)
WHERE DT = cast(getdate() as date)

--Le volume total de contacts dormeurs permanent optin 
--=> DORMEUR_POTENTIEL = 1 ET DORMEUR_PERMANENT = 1
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_OPTIN_DORMEUR_PERMANENT = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
where DORMEUR_PERMANENT = 1
and not exists (
	select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) 
	where T1.MAIL = T2.MAIL AND EMAIL = 0
)  and optout is null)
WHERE DT = cast(getdate() as date)


--La purge estimative mensuelle (au bout de 3 ans d’inactivité)
--=> Nb d'emails qui vont être purgés dans 1 mois 
-- dans 1 mois
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_PURGE_1M = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
where DERNIERE_ACTIVITE_DT < DATEADD(month,-35,GETDATE()) 
	and DERNIERE_ACTIVITE_DT >= DATEADD(month,-36,GETDATE())  )
WHERE DT = cast(getdate() as date)

--et nombre qui vont être purgés dans 3 mois
UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_PURGE_3M = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
where DERNIERE_ACTIVITE_DT < DATEADD(month,-33,GETDATE()) 
	and DERNIERE_ACTIVITE_DT >= DATEADD(month,-34,GETDATE())  )
WHERE DT = cast(getdate() as date)

-- on supprime les stats qui ont plus de 6 mois
DELETE FROM DATA_COMUNDI_KPI_DATASTUDIO
WHERE DT < dateadd(mm,-6,getdate());

-- on compte le nombre d'email créés dans les 7 derniers jours 
UPDATE T1  
SET NB_EMAIL_CREES = T2.NB
FROM DATA_COMUNDI_KPI_DATASTUDIO T1
INNER JOIN (
	select  cast(CREATED_DT as date) AS CREATED_DT, count(*) AS NB 
	from USERS_COMUNDI_EMAILS with (nolock) 
	WHERE cast(CREATED_DT as date) >= getdate()-7 
	GROUP BY cast(CREATED_DT as date)
) T2 ON T2.CREATED_DT = T1.DT
WHERE T1.DT >= getdate()-7


-- calcule des emails actifs dans les 12 derniers mois : NB_EMAIL_ACTIVITE_12M

UPDATE DATA_COMUNDI_KPI_DATASTUDIO 
SET NB_EMAIL_ACTIVITE_12M = (select count(*) from USERS_COMUNDI_EMAILS T1 with (nolock)
	where DERNIERE_ACTIVITE_DT > DATEADD(year,-1,GETDATE())  
	and optout is null
	and not exists (select 1 from DATA_COMUNDI_CONSENTEMENTS T2 with (nolock) where T1.MAIL = T2.MAIL AND EMAIL = 0)
	and DORMEUR_POTENTIEL IS NULL  )
WHERE DT = cast(getdate() as date)


--------------------------------------------------------------------------------
-------- TICKET #9443 ----------------------------------------------------------

-- on compte le nombre de leads créés dans les 7 derniers jours 
UPDATE T1  
SET NB_LEADS = T2.NB
FROM DATA_COMUNDI_KPI_DATASTUDIO T1
INNER JOIN (
	select  cast(CREATED_DT as date) AS CREATED_DT, count(*) AS NB 
	from DATA_COMUNDI_LEADS with (nolock) 
	WHERE cast(CREATED_DT as date) >= getdate()-7 
	and FORMULAIRE in (1, 2, 3, 4, 5, 6, 7, 8, 62, 76, 81, 83, 84, 85) 
	GROUP BY cast(CREATED_DT as date)
) T2 ON T2.CREATED_DT = T1.DT
WHERE T1.DT >= getdate()-7

-- on compte le nombre d'adresses emails créées dans les 7 derniers jours via les leads 
UPDATE T1  
SET NB_EMAILS_LEADS = T2.NB
FROM DATA_COMUNDI_KPI_DATASTUDIO T1
INNER JOIN (
	select  cast(CREATED_DT as date) AS CREATED_DT, count(DISTINCT MAIL_CODE) AS NB 
	from DATA_COMUNDI_LEADS T1 with (nolock) 
	WHERE FORMULAIRE in (1, 2, 3, 4, 5, 6, 7, 8, 62, 76, 81, 83, 84, 85) 
		AND cast(CREATED_DT as date) >= getdate()-7 
	and exists (
		select 1 
		from USERS_COMUNDI_EMAILS T2 
		WHERE T1.CREATED_DT>=cast(T2.CREATED_DT-1 as date)
	)
	GROUP BY cast(CREATED_DT as date)
) T2 ON T2.CREATED_DT = T1.DT
WHERE T1.DT >= getdate()-7

-- on compte le nombre d'adresses emails créées dans les 7 derniers jours via les leads 
-- ET QUI SONT OPTIN et pas OPTOUT tech
UPDATE T1  
SET NB_EMAILS_LEADS_OPTIN = T2.NB
FROM DATA_COMUNDI_KPI_DATASTUDIO T1
INNER JOIN (
	select  cast(CREATED_DT as date) AS CREATED_DT, count(DISTINCT MAIL_CODE) AS NB 
	from DATA_COMUNDI_LEADS T1 with (nolock) 
	WHERE FORMULAIRE in (1, 2, 3, 4, 5, 6, 7, 8, 62, 76, 81, 83, 84, 85) 
		AND cast(CREATED_DT as date) >= getdate()-7 
	and exists (
		select 1 
		from USERS_COMUNDI_EMAILS T2 
		WHERE T1.CREATED_DT>=cast(T2.CREATED_DT-1 as date)
		and optout is null 
		and not exists (select 1 from DATA_COMUNDI_CONSENTEMENTS T3 with (nolock) where T2.MAIL = T3.MAIL AND EMAIL = 0)
	)
	GROUP BY cast(CREATED_DT as date)
) T2 ON T2.CREATED_DT = T1.DT
WHERE T1.DT >= getdate()-7

-- #9443 : calcul du nombre de réactivation d'emails sur les 7 derniers jours
UPDATE T1  
SET NB_EMAILS_REACTIVES = T2.NB
FROM DATA_COMUNDI_KPI_DATASTUDIO T1
INNER JOIN (
	select REACTIVATION_DT, count(*) as NB from USERS_COMUNDI_EMAILS T1 with (nolock) 
	WHERE REACTIVATION_DT is not null
	group by REACTIVATION_DT) T2 ON T2.REACTIVATION_DT = T1.DT
WHERE T1.DT >= getdate()-7


-- sélection des données pour les 3 derniers mois pour export 
select 
FORMAT(DT,'yyyy-MM-dd') AS DT,
NB_EMAIL_TOTAL,
NB_EMAIL_OPTIN,
NB_EMAIL_OPTOUT,
NB_EMAIL_ACTIFS_4M,
NB_EMAIL_ACTIFS_4M_12M,
NB_EMAIL_ACTIFS_12M_PLUS,
NB_EMAIL_OPTIN_DORMEUR_POTENTIEL,
NB_EMAIL_OPTIN_DORMEUR_PERMANENT,
NB_EMAIL_PURGE_1M,
NB_EMAIL_PURGE_3M,
NB_EMAIL_ACTIVITE_12M,
NB_EMAIL_CREES,
NB_LEADS,
NB_EMAILS_LEADS,
NB_EMAILS_LEADS_OPTIN,
NB_EMAILS_REACTIVES
from DATA_COMUNDI_KPI_DATASTUDIO
where DT > dateadd(mm,-3,getdate())


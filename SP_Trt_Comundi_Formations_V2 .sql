AS
BEGIN
SET NOCOUNT ON;
MERGE DATA_COMUNDI_FORMATION AS T
USING (SELECT  
	TITRE,
	CODE_FORMATION,
	POSTIT,CATEGORIE,
	SS_CATEGORIE,
	CASE  
		WHEN URL_FINALE IS NULL THEN 'https://www.comundi.fr'
		ELSE URL_FINALE
	END AS URL_FINALE,
	SOUS_TITRE_WEB AS DESCRIPTION_COURTE,  
	left(DESCRIPTION_COURTE,300) AS SS_TITRE_WEB,
    ELIGIBLE_CPF,
    STATUT_WORKFLOW
	FROM TMP_COMUNDI_FORMATIONS WHERE CODE_FORMATION is not null and CODE_FORMATION <> '' )  AS S
ON (T.CODE_FORMATION = S.CODE_FORMATION)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT(CODE_FORMATION,
			TITRE,
			POSTIT, 
			CATEGORIE, 
			SS_CATEGORIE,
			URL_FINALE,
			DESCRIPTION_COURTE,
			SS_TITRE_WEB,
            ELIGIBLE_CPF,
            STATUT_WORKFLOW
		)
		VALUES(S.CODE_FORMATION,
			S.TITRE, 
			S.POSTIT,
			S.CATEGORIE, 
			S.SS_CATEGORIE,	
			S.URL_FINALE,
			S.DESCRIPTION_COURTE,
			S.SS_TITRE_WEB,
            S.ELIGIBLE_CPF,
            S.STATUT_WORKFLOW
		)
WHEN MATCHED
	THEN
		UPDATE SET
			T.TITRE=S.TITRE,
			T.POSTIT=S.POSTIT,
			T.CATEGORIE=S.CATEGORIE, 
			T.SS_CATEGORIE=S.SS_CATEGORIE,	
			T.URL_FINALE=S.URL_FINALE,
			T.DESCRIPTION_COURTE=S.DESCRIPTION_COURTE,
			T.SS_TITRE_WEB=S.SS_TITRE_WEB,
            T.ELIGIBLE_CPF=S.ELIGIBLE_CPF,
            T.STATUT_WORKFLOW=S.STATUT_WORKFLOW;
            
--- nettoyage des formations 

UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Formation : ','')
WHERE TITRE LIKE 'Formation : %'

UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Formation :','')
WHERE TITRE LIKE 'Formation :%'


UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Formation:','')
WHERE TITRE LIKE 'Formation:%'

---

UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Formation - ','')
WHERE TITRE LIKE 'Formation - %'

UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Formation -','')
WHERE TITRE LIKE 'Formation -%'


UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Formation-','')
WHERE TITRE LIKE 'Formation-%'
UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Formation- ','')
WHERE TITRE LIKE 'Formation- %'

UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Distanciel : ','')
WHERE TITRE LIKE 'Distanciel : %'
UPDATE DATA_COMUNDI_FORMATION 
SET TITRE = REPLACE(TITRE,'Distanciel :','')
WHERE TITRE LIKE 'Distanciel :%'


-- nettoyage des sous-titre web 
UPDATE DATA_COMUNDI_FORMATION 
SET SS_TITRE_WEB = NULL 
WHERE SS_TITRE_WEB='"';


MERGE ARTICLES_SESSION_FORMATION AS T
USING (SELECT TITRE,ARTICLES_SESSION_FORMATION.ID,DATA_COMUNDI_FORMATION.URL_FINALE,DATA_COMUNDI_FORMATION.SS_TITRE_WEB,DATA_COMUNDI_FORMATION.DESCRIPTION_COURTE
FROM DATA_COMUNDI_FORMATION
INNER JOIN ARTICLES_SESSION_FORMATION ON ARTICLES_SESSION_FORMATION.ID_FORMATION = DATA_COMUNDI_FORMATION.CODE_FORMATION  )  AS S
ON (T.ID = S.ID)
WHEN MATCHED
	THEN
		UPDATE SET
			T.NOM_FORMATION=S.TITRE,
			T.URL_FINALE=S.URL_FINALE,
			T.DESCRIPTION_COURTE=S.DESCRIPTION_COURTE,
			T.SS_TITRE_WEB=S.SS_TITRE_WEB;
END
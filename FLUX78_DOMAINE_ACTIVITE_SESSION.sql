-- gestion des domaines d'activité d'une session 

-- modification table TMP : ALTER TABLE dbo.Tmp_Comundi_DomainesActivites ADD ID_SESSION INT NULL, ID_INTERET INT NULL ;
ALTER TABLE Tmp_Comundi_DomainesActivites 
ADD ID_SESSION int,
ID_INTERET int;
  
-- mise à jour de la table Tmp_Comundi_DomainesActivites 
	-- pour mettre à jour l'ID session 
	UPDATE Tmp_Comundi_DomainesActivites 
		SET ID_SESSION =(SELECT TOP 1 ID FROM ARTICLES_SESSION_FORMATION WHERE Tmp_Comundi_DomainesActivites.OPA_OPERATION = ARTICLES_SESSION_FORMATION.ID_SESSION)
		, ID_INTERET= (SELECT TOP 1 ID FROM DATA_COMUNDI_DOMAINE_ACTIVITE WHERE DATA_COMUNDI_DOMAINE_ACTIVITE.CODE = Tmp_Comundi_DomainesActivites.OPA_ACTIVITE);
	-- pour mettre à jour l'ID DOMAINE ACTIVITE

-- gestion de l'effacement des données liées à des sessions présentes dans le FLUX
DELETE FROM DATA_COMUNDI_SESSIONS_INTERETS 
WHERE EXISTS (SELECT 1 FROM Tmp_Comundi_DomainesActivites WHERE Tmp_Comundi_DomainesActivites.ID_SESSION = DATA_COMUNDI_SESSIONS_INTERETS.ID_SESSION);


-- INSERTION DES NOUVELLES VALEURS
INSERT INTO DATA_COMUNDI_SESSIONS_INTERETS (ID_SESSION,ID_SESSION_LAM,ID_INTERET,ID_INTERET_LAM)
SELECT ID_SESSION,OPA_OPERATION,ID_INTERET,OPA_ACTIVITE FROM Tmp_Comundi_DomainesActivites
WHERE OPTI_REJECTED = 0;

-- mise à jour des ID Session Selligent dans la table d'atterissage pour ceux qui sont null si jamais il y a une désynchronisation. 
UPDATE DATA_COMUNDI_SESSIONS_INTERETS 
	SET ID_SESSION =(SELECT TOP 1 ID FROM ARTICLES_SESSION_FORMATION WHERE DATA_COMUNDI_SESSIONS_INTERETS.ID_SESSION_LAM = ARTICLES_SESSION_FORMATION.ID_SESSION)
	WHERE ID_SESSION IS NULL;

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE DBO.ARTICLES_SESSION_FORMATION AS T
	USING (SELECT DISTINCT TPS.ID_SESSION
			,LEFT(TPS.ID_SESSION,4) AS ID_FORMATION
			,TPS.NOM_SESSION AS NOM_SESSION
			,CONCAT(substring(DATE_DEBUT,7,4),'-',substring(DATE_DEBUT,4,2),'-',substring(DATE_DEBUT,1,2)) as DATE_DEBUT
			,CONCAT(substring(DATE_DEBUT,7,4),'-',substring(DATE_DEBUT,4,2),'-',substring(DATE_DEBUT,1,2)) as DATE_FIN
			,TPS.FONDAMENTALE
			,TPS.NB_IC
			,TPS.NB_JOURS
			,TPS.CENTRE_PROFIT
			,TPS.STATUT_SESSION
			,TPS.TYPE_SESSION
			,TPS.ZONE_GEO as ZONE_GEOGRAPHIQUE
			,TPS.MARQUE
			,TPS.NB_PLACE_RESTANTES
			,TPS.LIEU_SESSION_RS1
			,TPS.LIEU_SESSION_RS2
			,TPS.LIEU_SESSION_ADRESSE1
			,TPS.LIEU_SESSION_ADRESSE2
			,TPS.LIEU_SESSION_CP
			,TPS.LIEU_SESSION_VILLE
			,TPS.PETITE_SESSION
			,DATEPART(weekday, CONCAT(substring(TPS.DATE_DEBUT,7,4),'-',substring(TPS.DATE_DEBUT,4,2),'-',substring(TPS.DATE_DEBUT,1,2))) AS JOUR_DEBUT
			,DATA_COMUNDI_FORMATION.TITRE AS NOM_FORMATION
			,SESSION_GARANTIE
		FROM DBO.TMP_PRODUITSSESSION AS TPS
			LEFT OUTER JOIN DBO.DATA_CENTRE_PROFIT ON DATA_CENTRE_PROFIT.CODE = TPS.CENTRE_PROFIT
			LEFT JOIN DBO.DATA_COMUNDI_FORMATION ON TPS.ID_FORMATION = DATA_COMUNDI_FORMATION.CODE_FORMATION

		WHERE OPTI_REJECTED = 0
	) AS S
	ON (T.ID_SESSION = S.ID_SESSION)
	WHEN NOT MATCHED BY TARGET
		THEN
			INSERT(
			ID_SESSION
			,ID_FORMATION
			,NOM_SESSION
			,DATE_DEBUT
			,DATE_FIN
			,FONDAMENTALE
			,NB_IC
			,NB_JOURS
			,CENTRE_PROFIT
			,STATUT_SESSION
			,TYPE_SESSION
			,ZONE_GEOGRAPHIQUE
			,MARQUE
			,NB_PLACE_RESTANTES
			,LIEU_SESSION_RS1
			,LIEU_SESSION_RS2
			,LIEU_SESSION_ADRESSE1
			,LIEU_SESSION_ADRESSE2
			,LIEU_SESSION_CP
			,LIEU_SESSION_VILLE
			,PETITE_SESSION
			,JOUR_DEBUT
			,NOM_FORMATION
			,CREATED_DT
			,SESSION_GARANTIE)
		VALUES(S.ID_SESSION
			,S.ID_FORMATION
			,S.NOM_SESSION
			,S.DATE_DEBUT
			,S.DATE_FIN
			,S.FONDAMENTALE
			,S.NB_IC
			,S.NB_JOURS
			,S.CENTRE_PROFIT
			,S.STATUT_SESSION
			,S.TYPE_SESSION
			,S.ZONE_GEOGRAPHIQUE
			,S.MARQUE
			,S.NB_PLACE_RESTANTES
			,S.LIEU_SESSION_RS1
			,S.LIEU_SESSION_RS2
			,S.LIEU_SESSION_ADRESSE1
			,S.LIEU_SESSION_ADRESSE2
			,S.LIEU_SESSION_CP
			,S.LIEU_SESSION_VILLE
			,S.PETITE_SESSION
			,S.JOUR_DEBUT
			,S.NOM_FORMATION
			,GETDATE()
			,S.SESSION_GARANTIE)
	WHEN MATCHED
		THEN
			UPDATE SET T.NOM_SESSION = S.NOM_SESSION
			,T.ID_FORMATION=S.ID_FORMATION
			,T.DATE_DEBUT = S.DATE_DEBUT
			,T.DATE_FIN = S.DATE_FIN
			,T.FONDAMENTALE = S.FONDAMENTALE
			,T.NB_IC = S.NB_IC
			,T.NB_JOURS = S.NB_JOURS
			,T.CENTRE_PROFIT = S.CENTRE_PROFIT
			,T.STATUT_SESSION = S.STATUT_SESSION
			,T.TYPE_SESSION = S.TYPE_SESSION
			,T.ZONE_GEOGRAPHIQUE = S.ZONE_GEOGRAPHIQUE
			,T.MARQUE = S.MARQUE
			,T.NB_PLACE_RESTANTES=S.NB_PLACE_RESTANTES
			,T.LIEU_SESSION_RS1=S.LIEU_SESSION_RS1
			,T.LIEU_SESSION_RS2=S.LIEU_SESSION_RS2
			,T.LIEU_SESSION_ADRESSE1=S.LIEU_SESSION_ADRESSE1
			,T.LIEU_SESSION_ADRESSE2=S.LIEU_SESSION_ADRESSE2
			,T.LIEU_SESSION_CP=S.LIEU_SESSION_CP
			,T.LIEU_SESSION_VILLE=S.LIEU_SESSION_VILLE
			,T.PETITE_SESSION=S.PETITE_SESSION
			,T.JOUR_DEBUT=S.JOUR_DEBUT
			,T.NOM_FORMATION=S.NOM_FORMATION
			,T.CREATED_DT=GETDATE()
			,T.SESSION_GARANTIE = S.SESSION_GARANTIE;

END